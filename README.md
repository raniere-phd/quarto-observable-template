# Quarto Document Template

This is intented to short documents.
For longer documents, visit https://gitlab.com/raniere-phd/quarto-book-template.

Formats:

- HTML: https://raniere-phd.gitlab.io/quarto-document-template/
- DOCX: https://raniere-phd.gitlab.io/quarto-document-template/index.docx

![Screenshot showing title in HTML and DOCX.](inst/img/screenshot-title.png)

![Screenshot showing table in HTML and DOCX.](inst/img/screenshot-table.png)

![Screenshot showing figure in HTML and DOCX.](inst/img/screenshot-figure.png)

![Screenshot showing references in HTML and DOCX.](inst/img/screenshot-references.png)

## How to Use

First, clone this repository to your local machine.

### RStudio

[RStudio](https://www.rstudio.com/) has built-in support to Quarto.
Load the project with RStudio and you are ready to go.
Use the `Render` button to compile the Quarto file to HTML or DOCX.

### Docker

A `Dockerfile` and `docker-compose.yaml` is provided.
To compile the Quarto file to HTML, run

```
$ docker-compose up
```

To compile the Quarto file to DOCX, run

```
$ docker run quarto-document-template-web micromamba run ./tools/render-docx.sh
```



### GitLab CI

A `.gitlab-ci.yml` is provided
and
GitLab will compile the Quarto file to HTML and DOCX after every push.

## Top Tips

### Computed Tables

For the HTML, we recommend the use of paginated tables.
For the DOCX, we recommend the use of `knitr` default tables.

### Computed Figures

Here, we assume that you will use `ggplot2` without any extension.
For the HTML, we recommend the use of Plotly because it can translate the ggplot object to an interactive visualisation.
For the DOCX, we recomend the use of 300dpi PDF.

### Cross References

References to tables and figures are done by using `@tbl-` and `@fig-`.

You can customise the default behaviour of cross reference in the YAML header:

```
crossref:
  chapters: false
  fig-title: Figure
  tbl-title: Table
  title-delim: ":"
  fig-prefix: Figure
  tbl-prefix: Table
  ref-hyperlink: true
```

### References

References information are stored in a BibTeX file and the reference style is defined in a CSL file.
Both can be customise in the YAML header:

```
bibliography: references.bib
csl: csl/ieee-with-url.csl
```